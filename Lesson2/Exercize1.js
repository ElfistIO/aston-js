function makeObjectDeepCopy(originalObject) {
  let objectMap = new WeakMap();

  function initObjStucture(originalObject) {
    let data;
    let typeOfData = Object.prototype.toString.call(originalObject);
    switch (typeOfData) {
      case "[object Object]":
        data = Object.create(Object.getPrototypeOf(originalObject));
        break;
      case "[object Array]":
        data = [];
        break;
      case "[object Date]":
        data = new Date(originalObject);
        break;
      case "[object RegExp]":
        data = new RegExp(originalObject.source, originalObject.flags);
        break;
      default:
        data = originalObject;
    }
    return data;
  }

  function copyObjectToStructure(originalObject) {
    if (typeof originalObject === "object") {
      let data = initObjStucture(originalObject);
      if (objectMap.get(originalObject)) {
        return objectMap.get(originalObject);
      } else {
        objectMap.set(originalObject, data);
      }
      let keys = Reflect.ownKeys(originalObject);
      for (let i = 0; i < keys.length; i++) {
        let key = keys[i];
        data[key] = copyObjectToStructure(originalObject[key]);
      }
      return data;
    } else {
      return originalObject;
    }
  }

  return copyObjectToStructure(originalObject);
}
