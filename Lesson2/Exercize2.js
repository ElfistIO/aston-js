function selectFromInterval(arrayOfNumbers, firstInterval, secondInterval) {
  if (!Array.isArray(arrayOfNumbers)) {
    throw new Error("Первым аргументом передан не корректный тип данных!");
  }
  if (arrayOfNumbers.some((element) => typeof element !== "number")) {
    throw new Error("Тип данных в массиве не 'number'!");
  }
  if (
    (isNaN(firstInterval) && iaNaN(secondInterval)) ||
    firstInterval % 1 !== 0 ||
    secondInterval % 1 !== 0
  ) {
    throw new Error("Интервал не валидное число!");
  }
  if (secondInterval <= 0) return arrayOfNumbers.splice(0, firstInterval);
  if (firstInterval <= 0) return arrayOfNumbers.splice(0, secondInterval);
  if (firstInterval === secondInterval)
    return arrayOfNumbers.splice(firstInterval - 1).slice(0, 1);
  if ((firstInterval < secondInterval) & (firstInterval > 1)) {
    return arrayOfNumbers.splice(firstInterval - 1, secondInterval - 1);
  }
  if (firstInterval < secondInterval) {
    return arrayOfNumbers.splice(firstInterval - 1, secondInterval);
  }
  if ((firstInterval > secondInterval) & (secondInterval > 1)) {
    return arrayOfNumbers.splice(secondInterval - 1, firstInterval - 1);
  }
  if (firstInterval > secondInterval) {
    return arrayOfNumbers.splice(secondInterval - 1, firstInterval);
  }
  return arrayOfNumbers.splice(start, final);
}
