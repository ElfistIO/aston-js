class Calculator {
  constructor(displayPreviousValue, displayCurrentValue) {
    this.displayPreviousValue = displayPreviousValue;
    this.displayCurrentValue = displayCurrentValue;
    this.clear();
  }

  clear() {
    this.currentValue = "";
    this.previousValue = "";
    this.operation = undefined;
  }

  delete() {
    this.currentValue = this.currentValue.toString().slice(0, -1);
  }

  appendNumber(number) {
    if (number === "." && this.currentValue.includes(".")) return;
    if (this.currentValue.toString().length > 17) return;
    this.currentValue = this.currentValue.toString() + number.toString();
  }

  toggleSign() {
    Math.sign(this.currentValue) > 0
      ? (this.currentValue = `-${this.currentValue}`)
      : (this.currentValue = this.currentValue.toString().slice(1));
  }

  chooseOperation(operation) {
    if (this.currentValue === "") return;
    if (this.previousValue !== "") {
      this.compute();
    }
    switch (operation) {
      case "*":
        this.operation = "×";
        break;
      case "/":
        this.operation = "÷";
        break;
      default:
        this.operation = operation;
        break;
    }
    this.currentValue.toString().slice(-1) === "."
      ? (this.previousValue = this.currentValue.slice(
          0,
          this.currentValue.length - 1
        ))
      : (this.previousValue = this.currentValue);
    this.currentValue = "";
  }

  compute() {
    let computation;
    const prev = parseFloat(this.previousValue);
    const current = parseFloat(this.currentValue);
    if (isNaN(prev) || isNaN(current)) return;
    switch (this.operation) {
      case "+":
        computation = prev + current;
        break;
      case "-":
        computation = prev - current;
        break;
      case "×":
        computation = prev * current;
        break;
      case "÷":
        computation = prev / current;
        break;
      default:
        return;
    }
    this.currentValue = parseFloat(computation.toFixed(8));
    this.operation = undefined;
    this.previousValue = "";
  }

  getDisplayNumber(number) {
    const stringNumber = number.toString();
    const integerDigits = parseFloat(stringNumber.split(".")[0]);
    const decimalDigits = stringNumber.split(".")[1];
    let integerDisplay;
    if (isNaN(integerDigits)) {
      integerDisplay = "";
    } else {
      integerDisplay = integerDigits.toLocaleString("ru", {
        maximumFractionDigits: 0,
      });
    }
    if (decimalDigits != null) {
      return `${integerDisplay}.${decimalDigits}`;
    } else {
      return integerDisplay;
    }
  }

  updateDisplay() {
    this.displayCurrentValue.innerText = this.getDisplayNumber(
      this.currentValue
    );
    if (this.operation != null) {
      this.displayPreviousValue.innerText = `${this.getDisplayNumber(
        this.previousValue
      )} ${this.operation}`;
    } else {
      this.displayPreviousValue.innerText = "";
    }
  }
}

const displayPreviousValue = document.querySelector("[data-previous-value]");
const displayCurrentValue = document.querySelector("[data-current-value]");

const numberButtons = document.querySelectorAll("[data-number]");
const signButton = document.querySelector("[data-sign]");
const operationButtons = document.querySelectorAll("[data-operation]");
const equalsButton = document.querySelector("[data-equals]");
const deleteButton = document.querySelector("[data-delete]");
const clearButton = document.querySelector("[data-clear]");

const calculator = new Calculator(displayPreviousValue, displayCurrentValue);

numberButtons.forEach((button) => {
  button.addEventListener("click", () => {
    calculator.appendNumber(button.innerText);
    calculator.updateDisplay();
  });
});

operationButtons.forEach((button) => {
  button.addEventListener("click", () => {
    calculator.chooseOperation(button.innerText);
    calculator.updateDisplay();
  });
});

signButton.addEventListener("click", (button) => {
  calculator.toggleSign();
  calculator.updateDisplay();
});

equalsButton.addEventListener("click", (button) => {
  calculator.compute();
  calculator.updateDisplay();
});

clearButton.addEventListener("click", (button) => {
  calculator.clear();
  calculator.updateDisplay();
});

deleteButton.addEventListener("click", (button) => {
  calculator.delete();
  calculator.updateDisplay();
});

document.addEventListener("keydown", function (event) {
  let patternForNumbers = /[0-9]/g;
  let patternForOperators = /[+\-*\/]/gi;
  if (event.key.match(patternForNumbers)) {
    event.preventDefault();
    calculator.appendNumber(event.key);
    calculator.updateDisplay();
  }
  if (event.key === ".") {
    event.preventDefault();
    calculator.appendNumber(event.key);
    calculator.updateDisplay();
  }
  if (event.key.match(patternForOperators)) {
    event.preventDefault();
    calculator.chooseOperation(event.key);
    calculator.updateDisplay();
  }
  if (event.key === "Enter" || event.key === "=") {
    event.preventDefault();
    calculator.compute();
    calculator.updateDisplay();
  }
  if (event.key === "Backspace") {
    event.preventDefault();
    calculator.delete();
    calculator.updateDisplay();
  }
  if (event.key == "Delete") {
    event.preventDefault();
    calculator.clear();
    calculator.updateDisplay();
  }
});
