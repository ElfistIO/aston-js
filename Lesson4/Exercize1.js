function concatStrings(string, separator) {
  if (separator && typeof separator === "string") {
    return (nextString) => {
      if (typeof nextString !== "string") {
        return string;
      } else {
        return concatStrings(string + separator + nextString + separator);
      }
    };
  }
  return (nextString) => {
    if (typeof nextString !== "string") {
      return string;
    } else {
      return concatStrings(string + nextString);
    }
  };
}

//tests
console.log(concatStrings("first")("second")("third")());
console.log(concatStrings("first", null)("second")());
console.log(concatStrings("first", "123")("second")("third")());
console.log(concatStrings("some-value")("")("")(null));
console.log(concatStrings("some-value")(2));
console.log(concatStrings("some-value")("333")(123n));

// const infiniteCurry = fn => {
//   const next = (...args) => {
//     return x => {
//       if (!x) {
//         return args.reduce((acc, a) => {
//           return fn.call(fn, acc, a)
//         }, 0);
//       }
//       return next(...args, x);
//     };
//   };
//   return next();
// };

// function concatSpec(strs) {
//   let separator = "";
//   result = "";
//   for (let str of strs) {
//     if (str[0] && typeof str[0] !== "string") return result;
//     if (str[0]) result += str[0];
//     if (str[1]) separator = str[1];
//     result += separator;
//   }
//   return result;
// }

// let concatStrings = infiniteCurry(concatSpec);

// console.log(concatStrings("one"));
// console.log(concatStrings("one")("two"));
// console.log(concatStrings("one")("two")("three"));
