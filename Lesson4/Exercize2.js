class Calculator {
  isValid = (value) => {
    if (typeof value === "string" || typeof value === "bigint") return false;
    return isFinite(value) && !isNaN(value);
  };

  constructor(firstNumber, secondNumber) {
    if (!this.isValid(firstNumber) || !this.isValid(secondNumber)) {
      throw new Error("Ошибка ввода данных!");
    }

    this.firstNumber = firstNumber;
    this.secondNumber = secondNumber;

    this.setX = (num) => {
      this.setXInternal(num);
    };
    this.setY = (num) => {
      this.setYInternal(num);
    };
    this.logSum = () => {
      this.logSumInternal();
    };
    this.logMul = () => {
      this.logMulInternal();
    };
    this.logSub = () => {
      this.logSubInternal();
    };
    this.logDiv = () => {
      this.logDivInternal();
    };
  }

  setXInternal(num) {
    if (!this.isValid(num)) {
      throw new Error("Ошибка ввода данных!");
    }
    this.firstNumber = num;
  }
  setYInternal(num) {
    if (!this.isValid(num)) {
      throw new Error("Ошибка ввода данных!");
    }
    this.secondNumber = num;
  }
  logSumInternal() {
    console.log(this.firstNumber + this.secondNumber);
  }
  logMulInternal() {
    console.log(this.firstNumber * this.secondNumber);
  }
  logSubInternal() {
    console.log(this.firstNumber - this.secondNumber);
  }
  logDivInternal() {
    if (this.secondNumber === 0) {
      throw new Error("Нельзя делить на ноль!");
    }
    console.log(this.firstNumber / this.secondNumber);
  }
}

//tests
// const calculator = new Calculator(12, 3);
// calculator.logSum();
// calculator.logDiv();
// calculator.setX(15);
// calculator.logDiv();
// const logCalculatorDiv = calculator.logDiv;
// logCalculatorDiv();
// calculator.setY(444n);
