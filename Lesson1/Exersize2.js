const firstNum = Number(prompt("Введите число"));
const secondNum = Number(prompt("Введите число"));

if (isNaN(firstNum)) {
  alert("Некорректный ввод!");
} else {
  if (isNaN(secondNum)) {
    alert("Некорректный ввод!");
  } else {
    console.log(`Ответ: ${firstNum + secondNum}, ${firstNum / secondNum}.`);
  }
}
