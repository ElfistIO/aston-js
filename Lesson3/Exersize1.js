Array.prototype.myFilter = function myFilter(callback, context) {
  let filteredArr = [];
  for (let i = 0; i < this.length; i++) {
    if (callback.call(context, this[i], i, this)) {
      filteredArr.push(this[i]);
    }
  }
  return filteredArr;
};

//test

const testArray = [1, 20, 30, 80, 2, 9, 3];
const context = { limit: 20 };

console.log(
  testArray.myFilter(function (el) {
    return el >= this.limit;
  }, context)
);
